<?php

namespace Modules\Cambodium\database\seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Tag\Models\Cambodium;

class CambodiumDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        /*
         * Cambodia Seed
         * ------------------
         */

        // DB::table('cambodia')->truncate();
        // echo "Truncate: cambodia \n";

        Cambodium::factory()->count(20)->create();
        $rows = Cambodium::all();
        echo " Insert: cambodia \n\n";

        // Enable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
