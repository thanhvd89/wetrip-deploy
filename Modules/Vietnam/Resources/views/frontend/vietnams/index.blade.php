@extends('frontend.layouts.app')

@section('title') {{ __($module_title) }} @endsection

@section('content')

<div class="viet-nam">
    <section class="viet-nam__destination position-relative">
        <img class="viet-nam__destination--banner w-100" src="{{asset('img/Frame 483183.png')}}" alt="Frame 483183">
        <div class="viet-nam__destination--info container position-absolute start-50 translate-middle top-50 pt-5">
            <div class="row align-items-center">
                <div class="col-md-5 col-12 mb-3">
                    <span class="wt-font-merriweather wt-fw-300 fst-italic wt-fz-18 wt-color-gray">Destination</span>
                    <div class="d-flex mb-4 mt-3">
                        <p class="pr-3 wt-font-merriweather wt-fw-400 wt-fz-48 wt-color-gray">Vietnam</p>
                        <img src="{{asset('img/image 33.png')}}" alt="image 33">
                    </div>
                    <div class="d-flex flex-column">
                        <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                        <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm">Being located in South-East Asia, on the eastern margin of the Indochina Peninsula is our beautiful country – Vietnam, with its unique and worldwide well-known appearance – an S-shaped territory.</p>
                    </div>
                </div>
                <div class="col-md-7 col-12">
                    <div class="row images">
                        <div class="col-12 mb-3">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 2 (1).png')}}" alt="">
                        </div>
                        <div class="col-6">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 3 (1).png')}}" alt="">
                        </div>
                        <div class="col-4">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 6.png')}}" alt="">
                        </div>
                        <div class="col-2 position-relative">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 6.png')}}" alt="">
                            <div class="overlay">
                                <p class="d-flex justify-content-center align-items-center"><span class="text-white wt-fz-20 wt-fw-600">+10</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="viet-nam__content container py-md-5 py-4">
        <div class="d-flex flex-column align-items-center justify-content-center mb-5 pb-4">
            <div class="viet-nam__content--description d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-2">Being located in South-East Asia, on the eastern margin of the Indochina Peninsula is our beautiful country – Vietnam, with its unique and worldwide well-known appearance – an S-shaped territory.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base ">Vietnam is fortunately biased by nature, and gifted with various kinds of wonderful natural scenery. While travelling through Vietnam, you definitely cannot stop keeping your wide eyes on the treacherous chains of mountains along the country, the 3260 km-long coastlines with clear blue water and white sand, endless lands with golden paddy fields, spectacular rivers all over the country and spacious maroon deltas in the south-western area of Vietnam.</p>
            </div>
        </div>
        <div class="viet-nam__content--culture mb-5 pb-3">
            <div class="row d-flex align-items-center">
                <div class="col-lg-4 col-12 mb-3">
                    <img class="w-100" src="{{asset('img/image 1 (2).png')}}" alt="image 1 (1)">
                </div>
                <div class="col-lg-8 col-12">
                    <h5 class="wt-font-merriweather fw-bold fst-italic wt-fz-20 mb-3">Diversity Of Culture</h5>
                    <p class="wt-color-gray wt-fw-400 wt-fz-16 lh-lg">
                        It is not trumpeting to say Vietnam is a country having a variety of cultures. Our country has lasted for over 4000 years with the existence of civilizations and feudal dynasties, suffered through numerous up and down stages of history. In the past, we had a time of being the colony of China, and then France, however, Vietnamese people still preserved our tradition in the community, and adapted the cultures coming from overseas during the colonial period, to turn it into our own custom. Moreover, Vietnam has 54 ethnic groups living together, and each group has their own traditions and cultures. That makes the picture of Vietnam’s cultures really colorful and various
                    </p>
                </div>
            </div>
        </div>
        <div class="viet-nam__content--welcome mb-5 pb-3">
            <div class="row d-flex align-items-center">
                <div class="col-lg-8 col-md-6 col-12 mb-3">
                    <h5 class="wt-font-merriweather fw-bold fst-italic wt-fz-20 mb-3">Welcoming People</h5>
                    <p class="wt-color-gray wt-fw-400 wt-fz-16 lh-lg">
                        People are the most special element creating the uniqueness of Vietnam. From the brave and daring spirits of fighting against the enemies in the past, nowadays, the Vietnamese have been adored by travelers all around the world because of their hospitable and friendly characteristics. In our country, it is not hard to come across a person with a bright smile and warm attitude. They are also hard-working, truthful, and never stop losing their optimism.
                    </p>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <img class="w-100" src="{{asset('img/image 1 (2).png')}}" alt="image 1 (1)">
                </div>
            </div>
        </div>
        <div class="viet-nam__content--culture mb-5 pb-5">
            <div class="row d-flex align-items-center">
                <div class="col-lg-4 col-12 mb-3">
                    <img class="w-100" src="{{asset('img/image 1 (2).png')}}" alt="image 1 (1)">
                </div>
                <div class="col-lg-8 col-12">
                    <h5 class="wt-font-merriweather fw-bold fst-italic wt-fz-20 mb-3">Highlight Of The Country</h5>
                    <p class="wt-color-gray wt-fw-400 wt-fz-16 lh-lg">
                        With various geography and culture, Vietnam could offer a variety of activities, which will be suitable for different types of travelers. Specifically, for the family travelers, they could enjoy learning about the Vietnamese culture and daily life of local people, or choose a relaxing trip with staying at resorts and having fun at exciting theme parks. For the people who are interested in challenge and adventure, trekking in mountainous areas, climbing on perilous rocks, or surfing in the ocean. MICE tourism has also developed dramatically in Vietnam nowadays, because not only we have the suitable property for meetings, incentives, conferences and exhibitions, but also Vietnam could provide a wide range of entertainment activities, which would help the MICE tourism not get dull during their business trip. Tourism for education is also appropriate in Vietnam, due to our country having a rich tradition and historical relics, which are suitable for those who would like to study about culture.
                    </p>
                </div>
            </div>
        </div>
        <div class="w-100 position-relative wt-line-gradient"></div>
    </section>
    <section class="viet-nam__favorite container p-md-5 p-3">
        <div class="d-flex flex-column justify-content-center align-items-center mb-5">
            <img src="{{asset('img/map.png')}}" alt="">
            <div class="viet-nam__favorite--controls mt-4">
                <span data-slide="0" class="active item-1 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Hanoi</span>
                <span data-slide="1" class="item-2 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Ninh Binh</span>
                <span data-slide="2" class="item-3 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Halong & Hai Phong</span>
                <span data-slide="3" class="item-4 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Sapa</span>
                <span data-slide="4" class="item-5 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Hue</span>
                <span data-slide="5" class="item-6 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Danang</span>
                <span data-slide="6" class="item-7 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Hoi An</span>
                <span data-slide="7" class="item-8 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Nha Trang</span>
                <span data-slide="8" class="item-9 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Dalat</span>
                <span data-slide="9" class="item-10 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Ho Chi Minh City & Cu Chi Tunnel</span>
                <span data-slide="10" class="item-11 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Mekong Delta</span>
                <span data-slide="11" class="item-12 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Phu Quoc</span>
            </div>
        </div>
        <div class="viet-nam__favorite--slider position-relative">
            <div class="slide">
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/viet-nam-capital.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Hoankiem Lake</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Hanoi Capital</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/ninh-binh.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Ninh Binh</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/halong.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Halong & Hai Phong</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/sapa.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Sapa</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/hue.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Hue</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/danang.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Danang</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/hoian.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Hoi An</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/nhatrang.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Nha Trang</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/dalat.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Dalat</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/hcm.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Ho Chi Minh City & Cu Chi Tunnel</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/song-mekong.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Mekong Delta</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/phuquoc.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Phu Quoc</p>
                    </div>
                </div>
            </div>
            <button class="prev-btn position-absolute bottom-50">
                <i class="fa-solid fa-angle-left"></i>
            </button>
            <button class="next-btn position-absolute bottom-50">
                <i class="fa-solid fa-angle-right"></i>
            </button>
        </div>
        <div class="viet-nam__favorite--des mt-5">
            <div data-slide="0" class="d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Hanoi – our precious capital city is one of the most outstanding destinations for those who visit Vietnam. This over 1000-year old city has always been loved for its delicate vibe and nostalgic atmosphere. In Hanoi, there are numerous places that could make you reminisce about the old days, like the Old Quarter with long-lasting houses with Eastern architect marks, the French Quarter with elegant properties constructed by the French since the last century, sanctified temples and pagodas, and especially the Hoan Kiem Lake – the lake related to the legend about returning sword to the God of Ocean of King Le Thai To over 600 years ago. Hanoi is also a historic and cultural place with relics associated with our country’s formation, development and defense like Ba Dinh Square, Ho Chi Minh Complex, Temple of Literature, Maison Centrale (Hoa Lo prison), and a great number of museums restoring the priceless artefacts.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">The worthiest things to do in this city are visiting the old heritages to explore hidden stories, strolling along the streets or sitting on a cyclo to admire the serene scenery. You could visit the local market to find out the daily life, try the local street foods to discover the cuisine.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Moreover, along with the development of the world, Hanoi has gradually been transforming into a modern city, with more and more skyscrapers built. Standing on a building’s highest floor to observe the city with a panoramic view is also an interesting thing that you can experience when visit Hanoi.</p>
            </div>
            <div data-slide="1" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Located about 90-kilometer far to the south of Hanoi is Ninh Binh – one of the most famous destinations to visit in the North of Vietnam. Over 1000 years ago, this place was the ancient capital of Vietnam during two first feudal dynasties – the Dinh dynasty and the Le dynasty. Generally, Ninh Binh is surrounded by overlapping mountains, and embraced by gentle poetic rivers, which makes an impression of a sacred, mysterious and majestic land. Ninh Binh is well-known for its picturesque attractions, namely Trang An Scenic Landscape Complex which was recognized as the The World Natural Heritage by the UNESCO, Tam Coc dyed with the shades of golden with rice paddy fields along the banks river, the glorious limestones caves or antique Eastern temples and pagodas.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Coming to Ninh Binh, you would be captivated by numerous interesting things to do. Specifically, you could choose to take a boat trip at Tam Coc or Trang An complex, sit on a rural wooden boat with a local sailor to leisurely admire the landscape with wind in your hair and the relaxing sound of nature by your ears. You could also come to the historical relics, like Hoa Lu Temple, where is used to worship Dinh and Le King to know more about this land’s history, or challenge yourself by climbing up over 500 steps of the Mua cave to enjoy a whole scene of Ninh Binh when you goal the summit, or discovering the hidden caverns and valleys to discover the spectacular of creation.</p>
            </div>
            <div data-slide="2" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Heading East about 3 hours from Hanoi, you will reach Ha Long – an extremely gorgeous place located on the margin of northern Vietnam, considered as the pride of the Gulf of Tonkin. Ha Long Bay - the place designated twice as The World’s Natural Heritage in 1994 and 2006 by UNESCO also belongs to this city. Besides this well-known gifted place, we could also discover other magnificent bays, namely Lan Ha Bay and Bai Tu Long Bay. By looking down from above, these three bays are really eye-catching with the clear emerald sea surface reflecting the blue sky, as if it was an exquisite pearl created sophisticatedly by Mother Nature. This glowing cyan background is spotted by the greenery and moss grottoes, clever limestone caves and caverns and beautiful islands. All of these outstanding features makes people realize Ha Long at their very first glance.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Beautiful scenery with golden sand beaches, crystal clear water and splendid bays contribute to make Ha Long become the best place for leisure trips. Getting the cruise to visit all the corners of the bays, discover the grottoes, take part in the watery activities like swimming, sailing the kayak through the caverns. Visiting the islands with cycling is also a good way to enjoy the bay.</p>
            </div>
            <div data-slide="3" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Sapa is a mountainous town belonging to Lao Cai province, located in the north of Vietnam, which will take approximately 6 hours of transferring by road or overnight train from Hanoi.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">This town is really a one-of-a-kind destination of Vietnam. Located on the mountain, immersed in soft white clouds and silver haze make this town look like a fantastic dreamland. The high position makes the air chilling all the time, and the average temperature is only 15 Celsius per year.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Fansipan mountain, which is considered as the rooftop of Indochina is also located in Sapa. One of the most typical things to do in Sapa is transferring to the Fansipan peak by the cable car, or train, to once experience the feeling of hanging on the clouds, and to praise the splendor of the highest peak of our fatherland.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Sapa is the land of unspoiled landscapes. Due to its location on higher location, far from the crowded delta, people have not touched Sapa too much. That is the reason why this land still keeps its wild natural scenery with crystal clear water falls, soaring hills and mountains, and the fields of snowy buckwheat flowers all over the town. The picture of Sapa is also painted with more shades by ethnic minorities living here, with the lines of terrace fields where local people working, stilt houses on the cliffside, and the colorful outdoor weekly markets. These outdoor markets are not only the places for purchasing and trading, but also where people meet and greet, talk and gossip, and it has gradually become a typical culture of this mountainous town. Trekking through the rural villages, admiring the majestic natural scenery, strolling along the crowded markets, and experiencing the life of local people are the best things to do in Sapa.</p>
            </div>
            <div data-slide="4" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Lying tranquilly by the tender Perfume River, leaning leisurely over Ngu Binh mountain is antique Hue city. Hue is an imperial city, which had been our country’s old capital during the sovereign of the last feudal dynasty – the Nguyen dynasty until the 45s of the 20th century. This city has left many permanent impressions for visitors, because of its classic delicacy and melancholic beauty, as if it was a nostalgic person who always dreams of the golden ages in the far-away past. Even many years has slipped away, all the quintessence, tradition and cultures of this old imperial place have been still saved, preserved and lingered until nowadays. To feel deeply the magnificence of Hue, there will be nothing wonderful than spending a day on going through the timeless attractions like the majestic Imperial citadels, elegant Thien Mu pagoda, and the mysterious sacred tombs where the Emperors of Nguyen Dynasty rest for their longest sleep. Taking a wooden boat to flow along the Perfume river to see all the attractive and gentle sceneries leisurely in the sunset glow is also a great way to feel the gorgeousness of Hue city from the bottom of your heart.</p>
            </div>
            <div data-slide="5" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Da Nang – A beautiful metropolis located by the coastlines is widely known as “The worthiest city to live in Vietnam”. Being located in the middle of central Vietnam, Da Nang is appealing because of not only its peaceful atmosphere and delicacy with the romantic Han river flows along the city to meet the sea, elegant pagodas hidden behind impressive nature or a wild Son Tra peninsula with marvelous natural landscape, but also the lively lifestyle with the abuzz and crowded local markets.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Da Nang is also famous as “The city of bridges”. Bridges built crossing the Han river to combine different parts of the city have always been the must-see places of Da Nang. We have the Han River bridge – the first swinging bridge of Vietnam, Dragon bridge which was built with a golden dragon winding on the pylons, or eye-catching Tran Thi Ly Bridge with iridescent pylons lighting up the city in the night.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">The most famous image of Da Nang is definitely the Golden Bridge – a bridge covered in the bright golden shade, hung up in the air by stone hands. This bridge lies on the peak of Ba Na hills where has an extremely vivid theme park. Taking the cable car in about 25 minutes, and then go up the hill, the fantastic Golden Bridge will appear in front of your eyes, and keep you gazing all the time to its magical and sophisticated look.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">To be a favorite beach destination, Danang is home to numerous of hotels and resorts by the beach that can carter to variety of demands from local brand to international luxury chain, from capsule dorm to top notch properties giving the second-to-none experience for their guests.</p>
            </div>
            <div data-slide="6" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Taking about 45 minutes of driving to the south of Da Nang city is a land of heritages called Hoi An. Hoi An is the city belong to Quang Nam province, which have two cultural heritages prized by UNESCO – the Hoi An Ancient Town and the My Son Sanctuary.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Hoi An Ancient Town is really eye-catching with the chain of ancient houses with yellow walls and brown rooftops reflecting to the gentle Hoai river. In the past, this town was a crowded port where the commercial activities with foreigners, especially retailers coming from China and Japan happening frequently in 15th and 16th century. That is the reason why you could see that the architecture of Hoi An is affected strongly by Chinese and Japanese culture, but it is still combined harmoniously with our own features, which create a very unique Hoi An. Visiting Hoi An will offer us a chance to get lost in the previous safe and sound days with the Japanese-style wooden bridge lead to an old pagoda, ancient properties preserved originally, and a lively atmosphere of trading still lingering until nowadays. In the evening, the lights from colorful lanterns and glowing flower garlands released on the river light up this small town, which makes it look like a mythical land.</p>
            </div>
            <div data-slide="7" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Before being found by the French, Da Lat had been like a charming princess sleeping safe and sound in the magical curtain of hazy fog. In the 20th century, the French colonial authority discovered this land, and was impressed by its wild and poetic scenery, and they immediately turn it into their relaxing place during the summertime because of its cool weather.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Affected sharply by the Western architecture, Da Lat looks like a little Europe existing in Vietnam. Riding around the city, you could not stop gazing at Gothic churches, ancient and moss-covered French villas, delicate palaces belonging to King Bao Dai who was the last sovereign of Vietnam, or mysterious hill of verdure pines.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Da Lat is also famous for its wide variety of flowers. By visiting it, travelers may feel like they are lost in the flower land in the fairy tale “Thumbelina”. The comfortable weather creates a great environment for blossoms to bloom brilliantly. Indeed, in Da Lat, there are a lot of vast gardens full of colorful hydrangeas, scarlet roses, magnificent azaleas, marigold mimosas, and even the wild flowers on walls or growing up on the walking side also look pretty. All of them paint a picturesque and incandescent Da Lat in wanderers’ eyes.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">To enjoy this city, there will be nothing greater than visiting all the famous attractions like Bao Dai King’s three palaces, the Truc Lam Zen Monastery or the natural landscape like Datanla waterfall, or just simply chillaxing at a café and feel the peaceful atmosphere of this city.</p>
            </div>
            <div data-slide="8" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Before being found by the French, Da Lat had been like a charming princess sleeping safe and sound in the magical curtain of hazy fog. In the 20th century, the French colonial authority discovered this land, and was impressed by its wild and poetic scenery, and they immediately turn it into their relaxing place during the summertime because of its cool weather.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Affected sharply by the Western architecture, Da Lat looks like a little Europe existing in Vietnam. Riding around the city, you could not stop gazing at Gothic churches, ancient and moss-covered French villas, delicate palaces belonging to King Bao Dai who was the last sovereign of Vietnam, or mysterious hill of verdure pines.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Da Lat is also famous for its wide variety of flowers. By visiting it, travelers may feel like they are lost in the flower land in the fairy tale “Thumbelina”. The comfortable weather creates a great environment for blossoms to bloom brilliantly. Indeed, in Da Lat, there are a lot of vast gardens full of colorful hydrangeas, scarlet roses, magnificent azaleas, marigold mimosas, and even the wild flowers on walls or growing up on the walking side also look pretty. All of them paint a picturesque and incandescent Da Lat in wanderers’ eyes.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">To enjoy this city, there will be nothing greater than visiting all the famous attractions like Bao Dai King’s three palaces, the Truc Lam Zen Monastery or the natural landscape like Datanla waterfall, or just simply chillaxing at a café and feel the peaceful atmosphere of this city.</p>
            </div>
            <div data-slide="9" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Ho Chi Minh city, which is known as the biggest economic center of Vietnam, is the metropolis of the Southern area. This is a modern city with enormous skyscrapers and dazzling shopping malls. It is also famous for the energetic lifestyle with bustling streets every morning, crowded pedestrian streets, and busy night life. Being a part of the enthusiastic melody of this city is one of the best ways to enjoy this city.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Besides, Ho Chi Minh city still keeps the priceless value of an over 300-year old city with historic places, which still preserve the artifacts of the Vietnam war. Coming to Ho Chi Minh city, you could visit the Reunification Palace, which was the working and living place of the President of South Vietnam, and the War Remnants Museum restoring the real objects during the Vietnam War.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Moreover, about one and half hours of driving to the north is the Cu Chi Tunnels – an immense network underground, which was a secret stronghold of Vietnamese forces to shelter and plan the strategy of important military campaigns in the anti-Americas resistance war.</p>
            </div>
            <div data-slide="10" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">In the south western area of Vietnam is a unique region with vast delta owning an immense maze of rivers, swamps, canals and islands. This area is the downstream of Mekong River – the seventh greatest river of Mekong Delta, where the flowing will be divided into nine streams and run to the sea. That is the reason why this sector also called “Cuu Long” delta, which means nine dragons. As a result, waterways were once the main mean of transportation of the local here, especially where there were not many bridges built. Floating markets were therefore very popular in Southern provinces and become a cultural feature here where was very hectic and crowded with the local people coming to trade and exchange the goods and agricultural products very hectic. Cai Be, Cai Rang, and Phong Dien are the biggest ones.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Unlike a dynamic and bustling Ho Chi Minh City with more influenced by Western lifestyle, people in Mekong Delta are rustic and friendly. You are always welcomed by the warm smiles from people around you.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Blessed with fertile land, the southern delta is a rice bowl of the country as well as home to many picturesque pagodas, ethnic communities, and countless lush vegetable garden, orchards, fish farms. In the countryside tour, you will have great chance to experience the local life by visiting their family run factories producing coconut candy, rice paper, noodles, bricks; cool off in a lush garden to taste tropical fruits and honey tea, listening traditional folk music performed by the local people. This is definitely a one-of-a-kind experiences that you could not find anywhere.</p>
            </div>
            <div data-slide="11" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Located in the southern area, which is affected by the tropical weather, with the crystal water surrounding the territory, Phu Quoc – the very first island city of Vietnam has been well-known for many years as a paradise of beach tourism. The best time to visit this beautiful island is from November to April of next year, during the dry season of southern Vietnam, it rarely rains, and the weather is warm and sunny day by day.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Own bunches of luxury hotels and beach-front resorts and dozens of alluring white sand beaches; entertainment complexes like the Safari area with numerous species of animals; Grand World – The sleepless colorful small city that called as Eastern Venice; the exciting amusement parks like Vin Wonders theme parks with a great deal of carnival games and the Aquatopia water parks where could be reached by the Hon Thom cable car across the ocean which is designated as the longest cable car in the world, recorded by the Guinness; island hoping tour with many water activities like snorkeling, diving, or walking under the sea,.. there are so many things that one destination can offer so it is not exaggerated to say Phu Quoc is truly an ideal place that can please even the most demanding guests.</p>
            </div>
        </div>
        <div class="viet-nam__favorite--control-other my-5 px-md-5">
            <div class="w-100 pt-3 px-3 d-flex justify-content-between">
                <button class="prev wt-font-merriweather fw-bold wt-fz-14 px-3 py-2 wt-rounded-6" type="button"><i class="fa-solid fa-arrow-left-long pr-2"></i> Previous</button>
                <button class="next wt-font-merriweather fw-bold wt-fz-14 px-3 py-2 wt-rounded-6" type="button">Next <i class="fa-solid fa-arrow-right-long pl-2"></i></button>
            </div>
        </div>
    </section>
</div>

@endsection
