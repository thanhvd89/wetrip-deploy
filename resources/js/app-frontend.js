import './bootstrap';
import 'slick-carousel/slick/slick.min.js';
import $ from 'jquery';

$(document).ready(function () {
	$('.header .mobile .menu .hamburger').click(function () {
		$('.header .mobile .menu ul').toggleClass('d-none');
	});
	
	let centerPadding = '200px';
	if (window.innerWidth < 1440) {
		centerPadding = '100px';
	}
	if (window.innerWidth < 1200) {
		centerPadding = '50px';
	}
	if (window.innerWidth < 992) {
		centerPadding = '0px';
	}
	
	$(".home__favorite--slider .slide").slick({
		dots: true,
		infinite: true,
		centerMode: true,
		slidesToShow: 1,
		centerPadding: centerPadding,
		prevArrow: $(".home__favorite--slider .prev-btn"),
		nextArrow: $(".home__favorite--slider .next-btn"),
	});
	//======================= page viet-nam ===========================
	const slideElement = $(".viet-nam__favorite--slider .slide");
	const controlsElement = $('.viet-nam__favorite--controls span');
	
	slideElement.slick({
		dots: true,
		infinite: true,
		centerMode: true,
		slidesToShow: 1,
		centerPadding: centerPadding,
		prevArrow: $(".viet-nam__favorite--slider .prev-btn"),
		nextArrow: $(".viet-nam__favorite--slider .next-btn"),
	});
	controlsElement.click(function(){
		var slideIndex = $(this).data('slide');
		slideElement.slick('slickGoTo', slideIndex);
	});
	slideElement.on('afterChange', function(event, slick, currentSlide){
		controlsElement.removeClass('active');
		controlsElement.eq(currentSlide).addClass('active');
		
		//description controls
		$('.viet-nam__favorite--des div').addClass('d-none');
		$('.viet-nam__favorite--des div').eq(currentSlide).removeClass('d-none');
	});
	
	for (let i = 1; i <= 12; i++) {
		$(`.viet-nam__favorite--controls .item-${i}`).click(function () {
			$(`.viet-nam__favorite--controls .item-${i}`).addClass("active");
			for (let j = 1; j <= 12; j++) {
				if (j !== i) {
					$(`.viet-nam__favorite--controls .item-${j}`).removeClass("active");
				}
			}
		});
	}
	
	$('.viet-nam__favorite--control-other .next').click(function () {
		slideElement.slick('slickNext');
	});
	$('.viet-nam__favorite--control-other .prev').click(function () {
		slideElement.slick('slickPrev');
	});
	
	//=====================================================================
	
	//======================= page cambodia ===========================
	const slideCambodiaElement = $(".cambodia__favorite--slider .slide");
	const controlsCambodiaElement = $('.cambodia__favorite--controls span');
	
	slideCambodiaElement.slick({
		dots: true,
		infinite: true,
		centerMode: true,
		slidesToShow: 1,
		centerPadding: centerPadding,
		prevArrow: $(".cambodia__favorite--slider .prev-btn"),
		nextArrow: $(".cambodia__favorite--slider .next-btn"),
	});
	controlsCambodiaElement.click(function(){
		var slideIndex = $(this).data('slide');
		slideCambodiaElement.slick('slickGoTo', slideIndex);
	});
	slideCambodiaElement.on('afterChange', function(event, slick, currentSlide){
		controlsCambodiaElement.removeClass('active');
		controlsCambodiaElement.eq(currentSlide).addClass('active');
		
		//description controls
		$('.cambodia__favorite--des div').addClass('d-none');
		$('.cambodia__favorite--des div').eq(currentSlide).removeClass('d-none');
	});
	
	for (let i = 1; i <= 12; i++) {
		$(`.cambodia__favorite--controls .item-${i}`).click(function () {
			$(`.cambodia__favorite--controls .item-${i}`).addClass("active");
			for (let j = 1; j <= 12; j++) {
				if (j !== i) {
					$(`.cambodia__favorite--controls .item-${j}`).removeClass("active");
				}
			}
		});
	}
	
	$('.cambodia__favorite--control-other .next').click(function () {
		slideCambodiaElement.slick('slickNext');
	});
	$('.cambodia__favorite--control-other .prev').click(function () {
		slideCambodiaElement.slick('slickPrev');
	});
	
	//=====================================================================
	
	//======================= page cambodia ===========================
	const slideLaosElement = $(".laos__favorite--slider .slide");
	const controlsLaosElement = $('.laos__favorite--controls span');
	
	slideLaosElement.slick({
		dots: true,
		infinite: true,
		centerMode: true,
		slidesToShow: 1,
		centerPadding: centerPadding,
		prevArrow: $(".laos__favorite--slider .prev-btn"),
		nextArrow: $(".laos__favorite--slider .next-btn"),
	});
	controlsLaosElement.click(function(){
		var slideIndex = $(this).data('slide');
		slideLaosElement.slick('slickGoTo', slideIndex);
	});
	slideLaosElement.on('afterChange', function(event, slick, currentSlide){
		controlsLaosElement.removeClass('active');
		controlsLaosElement.eq(currentSlide).addClass('active');
		
		//description controls
		$('.laos__favorite--des div').addClass('d-none');
		$('.laos__favorite--des div').eq(currentSlide).removeClass('d-none');
	});
	
	for (let i = 1; i <= 12; i++) {
		$(`.laos__favorite--controls .item-${i}`).click(function () {
			$(`.laos__favorite--controls .item-${i}`).addClass("active");
			for (let j = 1; j <= 12; j++) {
				if (j !== i) {
					$(`.laos__favorite--controls .item-${j}`).removeClass("active");
				}
			}
		});
	}
	
	$('.laos__favorite--control-other .next').click(function () {
		slideLaosElement.slick('slickNext');
	});
	$('.laos__favorite--control-other .prev').click(function () {
		slideLaosElement.slick('slickPrev');
	});
	
	//=====================================================================
	
	//======================= page cambodia ===========================
	const slideMyanmarElement = $(".myanmar__favorite--slider .slide");
	const controlsMyanmarElement = $('.myanmar__favorite--controls span');
	
	slideMyanmarElement.slick({
		dots: true,
		infinite: true,
		centerMode: true,
		slidesToShow: 1,
		centerPadding: centerPadding,
		prevArrow: $(".myanmar__favorite--slider .prev-btn"),
		nextArrow: $(".myanmar__favorite--slider .next-btn"),
	});
	controlsMyanmarElement.click(function(){
		var slideIndex = $(this).data('slide');
		slideMyanmarElement.slick('slickGoTo', slideIndex);
	});
	slideMyanmarElement.on('afterChange', function(event, slick, currentSlide){
		controlsMyanmarElement.removeClass('active');
		controlsMyanmarElement.eq(currentSlide).addClass('active');
		
		//description controls
		$('.myanmar__favorite--des div').addClass('d-none');
		$('.myanmar__favorite--des div').eq(currentSlide).removeClass('d-none');
	});
	
	for (let i = 1; i <= 12; i++) {
		$(`.myanmar__favorite--controls .item-${i}`).click(function () {
			$(`.myanmar__favorite--controls .item-${i}`).addClass("active");
			for (let j = 1; j <= 12; j++) {
				if (j !== i) {
					$(`.myanmar__favorite--controls .item-${j}`).removeClass("active");
				}
			}
		});
	}
	
	$('.myanmar__favorite--control-other .next').click(function () {
		slideMyanmarElement.slick('slickNext');
	});
	$('.myanmar__favorite--control-other .prev').click(function () {
		slideMyanmarElement.slick('slickPrev');
	});
	
	//=====================================================================
	
	$(".home__affiliate--saying .slider").slick({
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 10000,
		prevArrow: $(".prev-btn1"),
		nextArrow: $(".next-btn1"),
	});
	
	for (let i = 1; i <= 4; i++) {
		$(`.our-services__content--theme .tour-images .image-${i}`).click(function () {
			$(`.our-services__content--theme .tour-images .image-${i}`).addClass("active");
			for (let j = 1; j <= 4; j++) {
				if (j !== i) {
					$(`.our-services__content--theme .tour-images .image-${j}`).removeClass("active");
				}
			}
		});
	}
});
