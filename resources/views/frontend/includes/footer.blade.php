<footer class="footer container">
    <div class="wt-line-gradient position-relative"></div>
    <div>
        <div class="row">
            <div class="col-md-6 col-12 mb-4 mt-5">
                <div class="footer__logo">
                    <img src="{{asset('img/Layer_1.png')}}" alt="Layer_1">
                    <p class="mt-4 wt-color-black-2 wt-fw-300">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-12 mt-5">
                <div class="row">
                    <div class="footer__wetrip col-6">
                        <p class="wt-color-blue-2 wt-fw-300">Wetrip</p>
                        <ul>
                            <li class="wt-cursor wt-color-black-2 wt-fw-300">Destination</li>
                            <li class="wt-cursor wt-color-black-2 wt-fw-300">Our Services</li>
                            <li class="wt-cursor wt-color-black-2 wt-fw-300">Typical Tour</li>
                            <li class="wt-cursor wt-color-black-2 wt-fw-300">Gallery</li>
                            <li class="wt-cursor wt-color-black-2 wt-fw-300">Contact Us</li>
                        </ul>
                    </div>
                    <div class="footer__follow-us col-6">
                        <p class="wt-color-blue-2 wt-fw-300">Follow Us</p>
                        <ul>
                            <li class="wt-cursor wt-color-black-2 wt-fw-300"><i class="fa-brands fa-facebook-f"></i>Facebook</li>
                            <li class="wt-cursor wt-color-black-2 wt-fw-300"><i class="fa-brands fa-twitter"></i>Twitter</li>
                            <li class="wt-cursor wt-color-black-2 wt-fw-300"><i class="fa-brands fa-instagram"></i>Instagram</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-lg-8 col-md-6 col-12 wt-color-black-2 wt-fz-14 wt-fw-300 mb-2">
                Copyright © 2023. Wetrip. All rights reserved.
            </div>
            <div class="col-lg-4 col-md-6 col-12 d-flex wt-color-black-2 wt-fz-14 wt-fw-300">
                <p class="pr-5">Terms & Conditions</p>
                <p>Privacy Policy</p>
            </div>
        </div>
    </div>
</footer>
